import argparse
import json
import sys
from publisherinsights import *

#=== Retrive Cross Platform Insights from API ===

"""
Our basis example demonstrates a basic query to the API, the items of
note here are the following:

* request to post our query
* request to retrieve query results
"""

parser = argparse.ArgumentParser()
parser.add_argument("access_token", help="A valid access token")
parser.add_argument("event_source_group_id", help="An event source group id")
args = parser.parse_args()

if args.access_token and args.event_source_group_id:
    access_token = args.access_token

    # Specify the event source group to be queried
    event_source_group_id = args.event_source_group_id
    criteria = PublisherInsightsQuery()
    # Specifying the start of the query date range
    criteria.since = 1494006387
    # Specifiying the end of the query date range
    criteria.until = 1496684787
    # Specifying the use of age and region breakdowns
    criteria.add_breakdown("age")
    criteria.add_breakdown("region")
    # Specifying the filtering criteria to be used in our request.
    criteria.add_filter({"name":"region", "operator":"eq", "values":["NY, US", "CA, US" ]})
    criteria.period = "week"
    pub_insights = PublisherInsights(access_token, event_source_group_id)

    # Post our request to query to the API
    post_response = pub_insights.query(criteria)
    # Extract `query_id` from JSON returned
    query_id = post_response['query_id']
    # request the results of our query
    query_results = pub_insights.fetch_results(query_id)
    print json.dumps(query_results, indent=4, sort_keys=True)
