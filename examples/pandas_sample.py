import argparse
import json
import sys
import pandas as pd
from publisherinsights import *

#=== Cross Platform Insights data into a Dataframe ===

"""
A basis example that demonstrates a basic query to the API, with a bit of
gentle massaging to convert the JSON into a usable format.
"""

#=== package_data ===
def package_data(data):
    df = pd.io.json.json_normalize(data,
                              record_path="rows",
                              meta="timestamp",
                              errors='ignore' )

    df['age'] =  df.apply(lambda x: x['breakdowns'][0], axis=1)
    df['region'] =  df.apply(lambda x: x['breakdowns'][1], axis=1)
    df['user_count'] =  df.apply(lambda x: x['aggregations'][0], axis=1)
    df['user_count'] = df['user_count'].apply(pd.to_numeric)
    df.drop(df.columns[[0, 1]], axis=1, inplace=True)

    return df

parser = argparse.ArgumentParser()
parser.add_argument("access_token", help="A valid access token")
parser.add_argument("event_source_group_id", help="An event source group id")
args = parser.parse_args()

if args.access_token and args.event_source_group_id:
    access_token = args.access_token

    # Specify the event source group to be queried
    event_source_group_id = args.event_source_group_id
    criteria = PublisherInsightsQuery()
    # Specifying the start of the query date range
    criteria.since = 1494006387
    # Specifiying the end of the query date range
    criteria.until = 1496684787
    criteria.add_breakdown("age")
    criteria.add_breakdown("region")
    # Specifying the filtering criteria to be used in our request.
    criteria.add_filter({"name":"region", "operator":"eq", "values":["NY, US", "CA, US" ]})
    criteria.period = "week"

    pub_insights = PublisherInsights(access_token, event_source_group_id)
    # Post our request to query to the API
    post_response = pub_insights.query(criteria)
    # Extract `query_id` from JSON returned
    query_id = post_response['query_id']

    data = pub_insights.fetch_results(query_id)["data"][0]["datapoints"]
    df = package_data(data)
    print df.head(5)
