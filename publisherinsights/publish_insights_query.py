import json

# A class to aid in building queries for the
# cross platform insights API
class PublisherInsightsQuery(object):
  def __init__(self):
      self._event_name = 'content_view'
      self._aggregations = ['users']
      self._filters = []
      self._breakdowns = []
      self._aggregation_period = None

  @property
  def since(self):
      return self._since

  @since.setter
  def since(self, value):
      self._since = value

  @property
  def until(self):
      return self._until

  @until.setter
  def until(self, value):
      self._until = value

  @property
  def aggregation_period(self):
      self._aggregation_period

  @aggregation_period.setter
  def aggregation_period(self,value):
      self._aggregation_period = value

  @property
  def filters(self):
      return self._filters

  @property
  def breakdowns(self):
      return self._breakdowns

  #=== add_filter ===
  def add_filter(self, filter):
      # Add a filter to the current query. Multiple filters behaves
      # as the intersection of filter results.
      self._filters.append(filter)

  #=== add_breakdown ===
  def add_breakdown(self, breakdown):
  # Add a breakdown to the current query. Multiple breakdown provide finer
  # granular understanding to the returned results.
      self.breakdowns.append(breakdown)

  #=== as_json ===
  def as_json(self):
  # Returns a valid JSON string represention of the query.
      return json.dumps(dict(self))

  def __iter__(self):
  # converts internal property names to API compliant JSON nodes.
      yield 'event_name', self._event_name
      yield 'aggregations', self._aggregations
      yield 'since', self.since
      yield 'until', self.until
      if self.aggregation_period:
          yield 'aggregation_period',self.aggregation_period
      if self.breakdowns:
          yield 'breakdowns', self.breakdowns
      if self.filters:
          yield 'filters', self.filters
