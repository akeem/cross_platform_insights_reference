import facebook
import json

class PublisherInsights(object):
     #add mutator for access token and ESGid

    def __init__(self, access_token, event_source_group_id):
        self.access_token = access_token
        self.event_source_group_id = event_source_group_id
        self.graph = facebook.GraphAPI(access_token=self.access_token,
                                       version='2.9')

   #=== query ===
    def query(self, criteria):
    # POST the request query to the Cross Platform Insights API
        post_args = {
           'fields': "status,datapoints,columns",
           'query': criteria.as_json()
        }

        return self.graph.request("{}/publisher_insights".\
            format(self.event_source_group_id), post_args=post_args)

    #=== fetch_results ===
    def fetch_results(self, query_id):
    # GET request to the API, returning the results of the query
        results_args = {'query_id': query_id, 'fields':'datapoints'}
        return self.graph.request("v2.9/{}/publisher_insights".\
            format(self.event_source_group_id), args = results_args)
