### Cross Platform Insights Reference Implementation

These samples are written in Python, but should be followable
none the less.

Getting Started:

1. Setup a virtual env for the project
    * `virtualenv crossplatform_insights`

2. Activate virtual environment
    * `source crossplatform_insights\bin\activate`

3. Install the project dependencies.
    * `pip install -r requirements.txt`

4. Explore the samples
    * `python -m examples.basic_sample <access token> <event source group id>`
    * `python -m examples.pandas_sample <access token> <event source group id>`

Documentation is provided in the `/docs` directory.
